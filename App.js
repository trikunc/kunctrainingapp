import React from 'react';
import {StyleSheet, View} from 'react-native';
import 'react-native-gesture-handler';
import firebase from '@react-native-firebase/app';

import Router from './src/navigations/navigation';

var firebaseConfig = {
  apiKey: "AIzaSyDVQXi1u_x4O-gZQS6rWzl1W4cCZ1debAY",
  authDomain: "reactsanber-3cc8e.firebaseapp.com",
  databaseURL: "https://reactsanber-3cc8e.firebaseio.com",
  projectId: "reactsanber-3cc8e",
  storageBucket: "reactsanber-3cc8e.appspot.com",
  messagingSenderId: "696356845841",
  appId: "1:696356845841:web:22f98fdd92ca7a49bd74b0",
  measurementId: "G-ZN7FSVN3YP"
};
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const App = () => {

  return (
    <Router />
  );
};

const styles = StyleSheet.create({});

export default App;
