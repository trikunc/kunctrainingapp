import React, { useState, useEffect } from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import SplashScreen from '../screens/SplashIntro/Splashscreen';
import Intro from '../screens/SplashIntro/Intro';
import Login from '../screens/Auth/Login';
import Register from '../screens/Auth/Register'
import Profile from '../screens/Tugas/Tugas2/Profile';
import Home from '../screens/Main/Home'
import Maps from '../screens/Maps'
import Chat from '../screens/Main/Chat'
import React_Native from '../screens/Charts/ReactNative'

import Icon from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();


const TabsScreen = () => (
  <Tab.Navigator 
    initialRouteName="Profile"
      tabBarOptions={{
        activeTintColor: '#3EC6FF',
        inactiveTintColor: 'gray',
      }}
  >
    <Tab.Screen 
      name="Profile" 
      component={Profile} 
      options={{
        tabBarLabel: 'Profile',
        tabBarIcon: ({color}) => (
          <MaterialCommunityIcons name="account" color={color} size={25} />
        ),
      }}
    />
    <Tab.Screen 
      name="Maps" 
      component={Maps}
      options={{
        tabBarLabel: 'Map',
        tabBarIcon: ({color}) => (
          <MaterialCommunityIcons name="map-search" color={color} size={25} />
        ),
      }}
      />
    <Tab.Screen 
      name="Chat" 
      component={Chat} 
      options={{
        tabBarLabel: 'Chat',
        tabBarIcon: ({color}) => (
          <MaterialCommunityIcons name="forum" color={color} size={25} />
        ),
      }}
      />
    <Tab.Screen 
      name="Home" 
      component={Home} 
      options={{
        tabBarLabel: 'Home',
        tabBarIcon: ({color}) => (
          <MaterialCommunityIcons name="home" color={color} size={25} />
        ),
      }}
      />
  </Tab.Navigator>
);



function AppNavigation() {
  const [isLoading, setIsLoading] = useState(true);
  const [showRealApp, setShowRealApp] = useState(false)
  const [loginApp, setLoginApp] = useState(false)
  // const [routeName, setRouteName] = useState("Intro")


  const AppStack = () => (
    <Stack.Navigator initialRouteName={
      showRealApp ? (loginApp ? "TabsScreen" : "Login") : "Intro"
      // if(loginApp){
      //   return "TabsScreen"
      // }
      }>
       <Stack.Screen
          name="Intro"
          component={Intro}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        /> 
        <Stack.Screen
          name="TabsScreen"
          component={TabsScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{headerShown: false}}
        /> 
        <Stack.Screen
          name="React_Native"
          component={React_Native}
          options={{headerShown: false}}
        /> 
    </Stack.Navigator>
  );

  //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
  useEffect(() => {
    AsyncStorage.getItem('first_time').then((value) => {
      // console.log('first_time:', value);
      setShowRealApp(value)
    })
    AsyncStorage.getItem('login_time').then((value) => {
      // console.log('login_time:', value);
      setLoginApp(value)
    })

    setTimeout(() => {
      setIsLoading(!isLoading);
    }, 1000);
  }, []);
  
  console.log('showRealApp:', showRealApp)
  console.log('loginApp:', loginApp)
  if (isLoading) {
    return <SplashScreen />;
  }

  // if (showRealApp && loginApp) {
  //   setRouteName("TabsScreen")
  // } else if (showRealApp) {
  //   setRouteName("Login")
  // } else {
  //   setRouteName("Intro")
  // }

  // if (showRealApp && loginApp) {
  //   return <TabsScreen />;
  // } else if (showRealApp) {
  //   return <Login />;
  // } else {
  //   return <Intro />;
  // }

  return (
    <NavigationContainer>
     <AppStack />
    </NavigationContainer>
  );
}

export default AppNavigation;
