import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import auth from '@react-native-firebase/auth';
import api from '../../api';
import {
  GoogleSignin,
  statusCodes,
  GoogleSigninButton,
} from '@react-native-community/google-signin';
import TouchID from 'react-native-touch-id'


const optionalConfigObject = {
  title: 'Authentication Required', // Android
  imageColor: '#191970', // Android
  imageErrorColor: '#ff0000', // Android
  sensorDescription: 'Touch sensor', // Android
  sensorErrorDescription: 'Failed', // Android
  cancelText: 'Cancel', // Android
  unifiedErrors: false, // use unified error messages (default false)
};



const Login = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const saveToken = async (token) => {
    try {
      await AsyncStorage.setItem('token', token);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    configureGoogleSignIn();
  }, []);

  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      offlineAccess: false,
      webClientId:
        '696356845841-lku2o3sf60p74t5raqbri9t2pmoh9d8e.apps.googleusercontent.com',
    });
  };

  const signInWithGoogle = async () => {
    try {
      const {idToken} = await GoogleSignin.signIn();
      console.log(idToken);

      const credentials = auth.GoogleAuthProvider.credential(idToken);

      auth().signInWithCredential(credentials);

      // navigation.navigate('TabsScreen');
      navigation.reset({
        index: 0,
        routes: [{ name: 'TabsScreen' }]
      })

      AsyncStorage.setItem("login_time", JSON.stringify(true))
    } catch (error) {
      console.log('error => ', error.message);
    }
  };

  const signInWithFingerprint = () => {
    console.log('Touch', TouchID)
    TouchID.authenticate('to demo this react-native component', optionalConfigObject)
      .then(success => {
      alert('Success')
      navigation.navigate('Register');
    })
    .catch(error => {
      console.log(error)
      alert('Failed')
    });
  };

  const onLoginPress = () => {
    return auth().signInWithEmailAndPassword(email, password)
    .then((res) => {
      // navigation.navigate('TabsScreen');
      navigation.reset({
        index: 0,
        routes: [{ name: 'TabsScreen' }]
      })
      AsyncStorage.setItem("login_time", JSON.stringify(true));
    })
    .catch((error) => {
      console.log(error)
    })
  }

  // const onLoginPress = () => {
  //   let data = {
  //     email,
  //     password,
  //   };
  //   Axios.post(`${api}/login`, data, {timeout: 20000})
  //     .then((res) => {
  //       // console.log('Login: ', res);
  //       const token = res.data.token;
  //       saveToken(token);
  //       navigation.navigate('TabsScreen');
  //       setEmail('');
  //       setPassword('');
  //     })
  //     .catch((err) => {
  //       console.log('Error: ', err);
  //       alert('Inputkan data dengan benar');
  //     });
  // };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
      <View style={styles.logoContainer}>
        <Image
          source={require('../../assets/images/logo.png')}
          style={styles.image}
        />
      </View>
      <TextInput
        style={styles.textInput}
        placeholder="Input Email here"
        placeholderTextColor="#abbabb"
        value={email}
        // required
        onChangeText={(value) => setEmail(value)}
      />
      <TextInput
        style={styles.textInput}
        placeholder="Input Password here"
        placeholderTextColor="#abbabb"
        value={password}
        secureTextEntry
        // required
        onChangeText={(value) => setPassword(value)}
      />
      <TouchableOpacity style={styles.button} onPress={() => onLoginPress()}>
        <Text>Hallo Login!</Text>
      </TouchableOpacity>
      <View style={styles.breakLine}>
        <Text style={styles.breakText}>Or</Text>
      </View>
      <View style={styles.gmailButton}>
        <GoogleSigninButton
          style={{width: '100%'}}
          onPress={() => signInWithGoogle()}
        />
      </View>
      <TouchableOpacity
        style={styles.buttonFinger}
        onPress={() => signInWithFingerprint()}
        >
        <Text style={{ color: '#fff'}}>Sign in with Fingerprint</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.buttonFinger}
        onPress={() => signInWithFingerprint()}
        >
        <Text style={{ color: '#fff'}}>Register</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    // justifyContent: 'center',
    backgroundColor: '#ffffff',
  },
  logoContainer: {
    // padding: 20,
    marginTop: 25,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'yellow',
  },
  image: {
    width: 200,
    height: 200,
  },
  textInput: {
    borderColor: '#000000',
    borderWidth: 1,
    width: '80%',
    marginVertical: 10,
  },
  button: {
    backgroundColor: '#3EC6FF',
    width: '80%',
    padding: 10,
    borderRadius: 12,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
  },
  buttonFinger: {
    backgroundColor: '#191970',
    width: '80%',
    padding: 10,
    borderRadius: 12,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
   
  },
  breakLine: {
    margin: 3,
  },
  breakText: {
    fontSize: 10,
  },
  gmailButton: {
    width: '80%',
    borderRadius: 12,
  },
  textHello: {
    fontSize: 12,
  },
});

export default Login;
