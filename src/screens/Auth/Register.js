import React, { useState } from 'react'
import {Image, Modal, StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native';

import { RNCamera, FaceDetector } from 'react-native-camera';
import storage from '@react-native-firebase/storage';
import Icon from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const Register = ({navigation}) => {

  const [isVisible, setIsVisible] = useState(false)
  const [type, setType] = useState('back')
  const [photo, setPhoto] = useState(null)

  const toggleCamera = () => {
    setType(type === 'back' ? 'front' : 'back')
  }

  const takePicture = async () => {
    const options = { quality: 0.5, base64: true }
    if(camera) {
      const data = await camera.takePictureAsync(options)
      console.log('data: ', data)
      setPhoto(data)
      setIsVisible(false)
    }
  }

  const uploadImage = (uri) => {
    const sessionId = new Date().getTime()
    return storage()
      .ref(`/images/${sessionId}`)
      .putFile(uri)
      .then((response) => {
        alert('Upload Success')
      })
      .catch((error) => {
        console.log(error)
      })
  }

  const renderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{ flex: 1 }}>
          <RNCamera
            style={{ display: 'flex', flex: 1, alignItems: 'center'}}
            ref={ref => {
              camera = ref;
            }}
            type={type}
          >
            <View style={styles.btnFlipContainer}>
              <TouchableOpacity style={styles.btnFlip} onPress={() => toggleCamera()}>
                <MaterialIcons name="rotate-3d-variant" size={15} />
              </TouchableOpacity>
            </View>
            <View style={styles.round} />
            <View style={styles.rectangle} />
            <View style={styles.btnTakeContainer}>
              <TouchableOpacity style={styles.btnTake} onPress={() => takePicture()}>
                <Icon name="camera" size={30} />
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Modal>
    )
  }

  return (
    <View style={styles.container}>
      <View style={styles.topView}>
        <Image
          style={styles.profileImage}
          source={photo === null ? require('../../assets/images/dummy-photo.png') : {uri: photo.uri}}
        />
        <TouchableOpacity style={styles.changePicture} onPress={() => setIsVisible()} >
          <Text style={styles.profileText}>Take Pic</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.bottomView}></View>
      <View style={styles.profileContainer}>
        <View style={styles.textContainer}>
          <Text>Nama</Text>
          <TextInput
            style={styles.textInput}
            placeholder="Input Name"
            placeholderTextColor="#abbabb"
            // value={name}
            // secureTextEntry
          />
        </View>
        <View style={styles.textContainer}>
          <Text>Email</Text>
          <TextInput
            style={styles.textInput}
            placeholder="Input Name"
            placeholderTextColor="#abbabb"
            // value={email}
            // secureTextEntry
          />
        </View>
        <View style={styles.textContainer}>
          <Text>Password</Text>
          <TextInput
            style={styles.textInput}
            placeholder="Input Name"
            placeholderTextColor="#abbabb"
            // value={password}
            secureTextEntry
          />
        </View>
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={() => uploadImage(photo.uri)}>
          <Text>Register</Text>
        </TouchableOpacity>
      </View>
      {renderCamera()}
    </View>
  )
}

export default Register

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  topView: {
    backgroundColor: '#3EC6FF',
    height: '40%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomView: {
    backgroundColor: 'white',
    height: '60%',
  },
  profileImage: {
    width: 70,
    height: 70,
    borderRadius: 35,
  },
  profileText: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 15,
  },
  changePicture: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  profileContainer: {
    display: 'flex',
    position: 'absolute',
    left: '10%',
    top: '35%',
    width: '80%',
    height: '40%',
    backgroundColor: '#fff',
    borderRadius: 8,
    padding: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  textContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    marginTop: 5,
  },
  textInput: {
    borderColor: '#abbabb',
    borderBottomWidth: 1,
    width: '100%',
    // marginBottom: 2,
  },
  buttonContainer: {
    display: 'flex',
    position: 'absolute',
    left: '10%',
    top: '75%',
    width: '80%',
  },
  button: {
    backgroundColor: '#3EC6FF',
    width: '100%',
    padding: 10,
    borderRadius: 12,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
  },
  round: {
    borderColor: 'white',
    borderWidth: 1,
    width: 150,
    height: 225,
    borderRadius: 75,
    marginTop: 50
  },
  rectangle: {
    marginTop:50,
    borderColor: 'white',
    borderWidth: 1,
    width: 150,
    height: 100,
  },
  btnFlip: {
    width: 30,
    height: 30,
    borderRadius: 15,
    backgroundColor: 'yellow',
    justifyContent: 'center',
    top: '80%',
    left: '-40%',
    alignItems: 'center',
  },
  btnTakeContainer: {
    justifyContent: 'center',
    marginTop: 50,
    alignItems: 'center',
    backgroundColor: 'yellow',
    width: 60,
    height: 60,
    borderRadius: 30,
    // justifySelf: 'center',
    marginHorizontal: 'auto'
  },
  btnTake: {
    fontSize: 50
  }
})
