import React, { useState, useEffect } from 'react'
import { 
  StyleSheet, 
  Text, 
  View,
} from 'react-native'
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import { GiftedChat } from 'react-native-gifted-chat'

const Chat = ({ route, navigation }) => {

  const [messages, setMessages] = useState([])
  const [user, setUser] = useState({})

  useEffect(() => {
    const user = auth().currentUser;
    setUser(user)
    getData()
    return () => {
      const db = database().ref('messages')
      if(db) {
        db.off()
      }
    }
  }, [])

  const getData = () => {
    database().ref('messages').limitToLast(20).on('child_added', snapshot => {
      const value = snapshot.val()
      setMessages(previousMessages => GiftedChat.append(previousMessages, value))
    })
  }

  const onSend = (( messages = [] ) => {
    for(let i=0; i<messages.length; i++) {
      database().ref('messages').push({
        _id: messages[i]._id,
        createdAt: database.ServerValue.TIMESTAMP,
        text: messages[i].text,
        user: messages[i].user
      })
    }
  })

  return (
    <GiftedChat 
    messages={messages}
    onSend={messages => onSend(messages)}
      user={{
        _id: user.uid,
        name: user.email,
        avatar: "lh3.googleusercontent.com/ogw/ADGmqu-o3zxrNpSemTM1DhpIb43DetnmcXXI4VebaVX_=s83-c-mo"
      }}
    />
  )
}

export default Chat

const styles = StyleSheet.create({})
