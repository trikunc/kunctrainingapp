import React from 'react'
import { Image, StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'

const Home = ({navigation}) => {
  return (
    <ScrollView style={styles.container}>

      <View style={styles.headerContainer}>
        <View style={styles.headerTop}>
          <Text style={styles.textTitle}>Kelas</Text>
        </View>
        <View style={styles.headerBot}>
          <TouchableOpacity style={styles.iconsContainer} onPress={() => navigation.navigate("React_Native")} >
            <Icon style={styles.icon} name="logo-react" ></Icon>
            <Text style={styles.textIcon}>React Native</Text>
          </TouchableOpacity>
          <View style={styles.iconsContainer}>
            <Icon style={styles.icon} name="logo-python" ></Icon>
            <Text style={styles.textIcon}>Data Science</Text>
          </View>
          <View style={styles.iconsContainer}>
            <Icon style={styles.icon} name="logo-react" ></Icon>
            <Text style={styles.textIcon}>React Js</Text>
          </View>
          <View style={styles.iconsContainer}>
            <Icon style={styles.icon} name="logo-laravel" ></Icon>
            <Text style={styles.textIcon}>Laravel</Text>
          </View>
        </View>
      </View>

      <View style={styles.headerContainer}>
        <View style={styles.headerTop}>
          <Text style={styles.textTitle}>Kelas</Text>
        </View>
        <View style={styles.headerBot}>
          <View style={styles.iconsContainer}>
            <Icon style={styles.icon} name="logo-wordpress" ></Icon>
            <Text style={styles.textIcon}>Wordpress</Text>
          </View>
          <View style={styles.iconsContainer}>
            <Image style={{width: 40, height: 40,}} source={require('../../assets/icons/website-design.png')} />
            <Text style={styles.textIcon}>Design Grafis</Text>
          </View>
          <View style={styles.iconsContainer}>
            <MaterialIcon style={styles.icon} name="server" ></MaterialIcon>
            <Text style={styles.textIcon}>Web Server</Text>
          </View>
          <View style={styles.iconsContainer}>
          <Image style={{width: 40, height: 40,}} source={require('../../assets/icons/ux.png')} />
            <Text style={styles.textIcon}>UI/UX Design</Text>
          </View>
        </View>
      </View>

      <View style={styles.headerContainer}>
        <View style={styles.headerTop}>
          <Text style={styles.textTitle}>Summary</Text>
        </View>
          
          <View>
            <View style={styles.titleCard}>
              <Text style={styles.textTitleCard}>React Native</Text>
            </View>
            <View style={styles.contentCard}>
              <View style={styles.content}>
                <Text style={styles.textContent}>Today</Text>
                <Text style={styles.textContent}>20 orang</Text>
              </View>
              <View style={styles.content}>
                <Text style={styles.textContent}>Total</Text>
                <Text style={styles.textContent}>100 orang</Text>
              </View>
            </View>
          </View>

          <View>
            <View style={styles.titleCard}>
              <Text style={styles.textTitleCard}>Data Science</Text>
            </View>
            <View style={styles.contentCard}>
              <View style={styles.content}>
                <Text style={styles.textContent}>Today</Text>
                <Text style={styles.textContent}>30 orang</Text>
              </View>
              <View style={styles.content}>
                <Text style={styles.textContent}>Total</Text>
                <Text style={styles.textContent}>100 orang</Text>
              </View>
            </View>
          </View>

          <View>
            <View style={styles.titleCard}>
              <Text style={styles.textTitleCard}>React Js</Text>
            </View>
            <View style={styles.contentCard}>
              <View style={styles.content}>
                <Text style={styles.textContent}>Today</Text>
                <Text style={styles.textContent}>66 orang</Text>
              </View>
              <View style={styles.content}>
                <Text style={styles.textContent}>Total</Text>
                <Text style={styles.textContent}>100 orang</Text>
              </View>
            </View>
          </View>

          <View>
            <View style={styles.titleCard}>
              <Text style={styles.textTitleCard}>Laravel</Text>
            </View>
            <View style={styles.contentCard}>
              <View style={styles.content}>
                <Text style={styles.textContent}>Today</Text>
                <Text style={styles.textContent}>60 orang</Text>
              </View>
              <View style={styles.content}>
                <Text style={styles.textContent}>Total</Text>
                <Text style={styles.textContent}>100 orang</Text>
              </View>
            </View>
          </View>

          <View>
            <View style={styles.titleCard}>
              <Text style={styles.textTitleCard}>Wordpress</Text>
            </View>
            <View style={styles.contentCard}>
              <View style={styles.content}>
                <Text style={styles.textContent}>Today</Text>
                <Text style={styles.textContent}>20 orang</Text>
              </View>
              <View style={styles.content}>
                <Text style={styles.textContent}>Total</Text>
                <Text style={styles.textContent}>100 orang</Text>
              </View>
            </View>
          </View>

          <View>
            <View style={styles.titleCard}>
              <Text style={styles.textTitleCard}>Design Grafis</Text>
            </View>
            <View style={styles.contentCard}>
              <View style={styles.content}>
                <Text style={styles.textContent}>Today</Text>
                <Text style={styles.textContent}>20 orang</Text>
              </View>
              <View style={styles.content}>
                <Text style={styles.textContent}>Total</Text>
                <Text style={styles.textContent}>100 orang</Text>
              </View>
            </View>
          </View>

          <View>
            <View style={styles.titleCard}>
              <Text style={styles.textTitleCard}>Web Server</Text>
            </View>
            <View style={styles.contentCard}>
              <View style={styles.content}>
                <Text style={styles.textContent}>Today</Text>
                <Text style={styles.textContent}>20 orang</Text>
              </View>
              <View style={styles.content}>
                <Text style={styles.textContent}>Total</Text>
                <Text style={styles.textContent}>100 orang</Text>
              </View>
            </View>
          </View>

          <View>
            <View style={styles.titleCard}>
              <Text style={styles.textTitleCard}>UI/UX Design</Text>
            </View>
            <View style={styles.contentCard}>
              <View style={styles.content}>
                <Text style={styles.textContent}>Today</Text>
                <Text style={styles.textContent}>20 orang</Text>
              </View>
              <View style={styles.content}>
                <Text style={styles.textContent}>Total</Text>
                <Text style={styles.textContent}>100 orang</Text>
              </View>
            </View>
          </View>

        {/* </ScrollView> */}

      </View>

      <View style={{height: 5}}></View>

    </ScrollView>
  )
}

export default Home

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  headerContainer: {
    marginBottom: 15,
    borderRadius: 8,
    overflow: 'hidden'
  },
  headerTop: {
    backgroundColor: '#088dc4',
    padding: 5,
  },
  headerBot: {
    backgroundColor: '#3EC6FF',
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textTitle: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 16
  },
  iconsContainer: {
    padding: 5,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-evenly'
  },
  icon: {
    color: '#FFFFFF',
    fontSize: 40
  },
  textIcon: {
    color: '#FFFFFF',
    fontSize: 12,
  },
  titleCard: {
    backgroundColor: '#3EC6FF',
    paddingVertical: 5,
    paddingHorizontal: 15,
  },
  textTitleCard: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    
  },
  contentCard: {
    backgroundColor: '#088dc4',
    padding: 5,
    flexDirection: 'column',
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20
  },
  textContent: {
    color: '#FFFFFF',
  }

    

})
