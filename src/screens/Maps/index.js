import React, { useEffect } from 'react'
import { FlatList, StyleSheet, Text, View } from 'react-native'
import MapboxGL from '@react-native-mapbox-gl/maps'

MapboxGL.setAccessToken('pk.eyJ1IjoidHJpa3VuYyIsImEiOiJja2c3Y2NoZ2gwNXdnMnJrY3g0MGc5bGo4In0.14gE3GZ0OjKhiBC5fE7YtQ')

const coor = [
  [107.598827, -6.896191],
  [107.596198, -6.899688],
  [107.618767, -6.902226],
  [107.621095, -6.898690],
  [107.615698, -6.896741],
  [107.613544, -6.897713],
  [107.613697, -6.893795],
  [107.610714, -6.891356],
  [107.605468, -6.893124],
  [107.609180, -6.898013]
]

const Maps = () => {

  useEffect(() => {
    const getLocation = async () => {
      try {
        const permissions = await MapboxGL.requestAndroidLocationPermissions()
      } catch (err) {
        console.log(err)
      }
    }
    getLocation()
  }, [])
  return (
    <View style={styles.container}>
      <MapboxGL.MapView style={{flex: 1}}>
        <MapboxGL.UserLocation visible={true} />
        <MapboxGL.Camera followUserLocation={true} />
        {coor.map((item, key) => {
          return (
            <MapboxGL.PointAnnotation
              key={key}
              id="pointAnotation"
              coordinate={item}
              title=''
            >
              <MapboxGL.Callout title={`Longitude: ${item[0]}, Latitude: ${item[1]}`} />
            </MapboxGL.PointAnnotation>
          )
        })}
      </MapboxGL.MapView>
    </View>
  )
}

export default Maps

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
})
