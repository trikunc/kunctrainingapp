import React, {useState, createContext} from 'react';
import {View, Text} from 'react-native';
import ContactApp from './ContactApp';

export const RootContext = createContext();

const Context = () => {
  const [input, setInput] = useState('');
  const [inputNum, setInputNum] = useState('');
  const [todos, setTodos] = useState([]);

  const handleChangeInput = (value) => {
    setInput(value);
  };
  const handleChangeNumber = (value) => {
    setInputNum(value);
  };

  const addTodo = () => {
    if (!input.trim()) {
      alert('Please Enter Name');
      return;
    } else if (!inputNum.trim()) {
      alert('Please Enter Phone Number');
      return;
    } else {
      const day = new Date().getDate();
      const month = new Date().getMonth();
      const year = new Date().getFullYear();
      const today = `${day}/${month}/${year}`;
      const key = Date.now().toString();
      setTodos([...todos, {title: input, No: inputNum, date: today, key: key}]);
      setInput('');
      setInputNum('');
    }
  };

  const deleteTodo = (id) => {
    setTodos(
      todos.filter((todos) => {
        if (todos.key !== id) return true;
      }),
    );
  };

  return (
    <RootContext.Provider
      value={{
        input,
        inputNum,
        todos,
        handleChangeInput,
        handleChangeNumber,
        addTodo,
        deleteTodo,
      }}>
      <ContactApp />
    </RootContext.Provider>
  );
};

export default Context;
