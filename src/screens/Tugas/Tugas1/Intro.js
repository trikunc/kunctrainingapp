import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const Intro = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.textHello}>
        Hello Kelas React Native Lanjutan Sanbercode!
      </Text>
    </View>
  );
};

export default Intro;

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    alignItems: 'center',
  },
  textHello: {
    fontSize: 12,
  },
});
