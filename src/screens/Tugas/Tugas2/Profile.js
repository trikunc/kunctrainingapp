import React, {useEffect, useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import api from '../../../api';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';

const Profile = ({navigation}) => {
  const [userInfo, setUserInfo] = useState(null);

  // useEffect(() => {
  //   async function getToken() {
  //     try {
  //       const token = await AsyncStorage.getItem('token');
  //       return getVenue(token);
  //       // console.log('token: ', token);
  //     } catch (err) {
  //       console.log(err);
  //     }
  //   }
  //   getCurrentUser();
  //   getToken();
  // }, []);

  const getCurrentUser = async () => {
    const userInfo = await GoogleSignin.signInSilently();
    console.log('userInfo: ', userInfo);
    console.log('userInfo: ', userInfo.user.photo);
    setUserInfo(userInfo);
  };

  const getVenue = (token) => {
    Axios.get(`${api}/venues`, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer ' + token,
      },
    })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onLogoutPress = async () => {
    try {
      AsyncStorage.removeItem("login_time")
      // AsyncStorage.removeItem("first_time")
      // navigation.navigate('Login');
      navigation.reset({
        index: 0,
        routes: [{ name: 'Login' }]
      })
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      // await AsyncStorage.removeItem('token');
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.topView}>
        <Image
          style={styles.profileImage}
          source={{
            uri: userInfo && userInfo.user && userInfo.user.photo,
          }}
        />
        <Text style={styles.profileText}>{userInfo && userInfo.user && userInfo.user.name}</Text>
      </View>
      <View style={styles.bottomView}></View>
      <View style={styles.profileContainer}>
        <View style={styles.textContainer}>
          <Text>Tanggal Lahir</Text>
          <Text>17 Agustus 1945</Text>
        </View>
        <View style={styles.textContainer}>
          <Text>Jenis Kelamin</Text>
          <Text>Laki-laki</Text>
        </View>
        <View style={styles.textContainer}>
          <Text>Hobi</Text>
          <Text>Traveling</Text>
        </View>
        <View style={styles.textContainer}>
          <Text>No. Telp</Text>
          <Text>081234567890</Text>
        </View>
        <View style={styles.textContainer}>
          <Text>Email</Text>
          <Text>{userInfo && userInfo.user && userInfo.user.email}</Text>
        </View>
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={() => onLogoutPress()}>
          <Text>Hallo Logout!</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  topView: {
    backgroundColor: '#3EC6FF',
    height: '40%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomView: {
    backgroundColor: 'white',
    height: '60%',
  },
  profileImage: {
    width: 70,
    height: 70,
    borderRadius: 35,
  },
  profileText: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 15,
  },
  profileContainer: {
    display: 'flex',
    position: 'absolute',
    left: '10%',
    top: '35%',
    width: '80%',
    height: '40%',
    backgroundColor: '#fff',
    borderRadius: 8,
    padding: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  textContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  buttonContainer: {
    display: 'flex',
    position: 'absolute',
    left: '10%',
    top: '75%',
    width: '80%',
  },
  button: {
    backgroundColor: '#3EC6FF',
    width: '100%',
    padding: 10,
    borderRadius: 12,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
  },
});
