import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import moment from 'moment';

const TodoList = () => {
  const [value, setValue] = useState('');
  const [todos, setTodos] = useState([]);

  const addTodo = () => {
    if (value.length > 0) {
      setTodos([...todos, {text: value, key: Date.now(), checked: false}]);
      setValue('');
    }
  };

  const deleteTodo = (id) => {
    setTodos(
      todos.filter((todo) => {
        if (todo.key !== id) return true;
      }),
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.titleHeader}>Masukkan Todo List</Text>
      </View>
      <View style={styles.textInputContainer}>
        <TextInput
          style={styles.textInput}
          multiline={true}
          placeholder="Input here"
          placeholderTextColor="#abbabb"
          value={value}
          onChangeText={(value) => setValue(value)}
        />
        <TouchableOpacity onPress={() => addTodo()}>
          <Icon name="plus" size={30} color="black" style={{marginLeft: 15}} />
        </TouchableOpacity>
      </View>
      <ScrollView style={{width: '100%'}}>
        {todos.map((item) => (
          <List
            text={item.text}
            key={item.key}
            deleteTodo={() => deleteTodo(item.key)}
          />
        ))}
      </ScrollView>
    </View>
  );
};

const List = (props) => {
  var date = moment().utcOffset('+05:30').format('MM/DD/YYYY');
  console.log(date);
  return (
    <View style={styles.listContainer}>
      <View style={styles.listText}>
        <Text style={styles.listDate}>{date}</Text>
        <Text style={styles.listItem}>{props.text}</Text>
      </View>
      <Icon
        name="trash-2"
        size={30}
        color="red"
        style={{marginLeft: 'auto', padding: 5}}
        onPress={props.deleteTodo}
      />
    </View>
  );
};

export default TodoList;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: '#F5FCFF',
    padding: 10,
  },
  header: {
    marginTop: 10,
    paddingBottom: 10,
  },
  titleHeader: {
    fontSize: 16,
    color: 'black',
  },
  textInputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: 'black',
    borderWidth: 1,
    paddingRight: 10,
    color: 'black',
  },
  textInput: {
    flex: 1,
    height: 20,
    fontSize: 18,
    fontWeight: 'bold',
    color: '#000',
    paddingLeft: 10,
    minHeight: '3%',
    height: 40,
  },
  listContainer: {
    marginTop: 15,
    flexDirection: 'row',
    borderColor: '#aaaaaa',
    borderWidth: 1,
    width: '100%',
    alignItems: 'stretch',
    minHeight: 40,
  },
  listText: {
    display: 'flex',
    flexDirection: 'column',
  },
  listDate: {
    paddingLeft: 10,
  },
  listItem: {
    paddingLeft: 10,
    fontSize: 17,
    color: 'black',
  },
});
