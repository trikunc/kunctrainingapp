import React, {useState, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  ScrollView,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import moment from 'moment';
import {RootContext} from '../Tugas4';

const TodoList = () => {
  const state = useContext(RootContext);
  console.log(state);

  const handleChange = (e) => state.handleChangeInput(e);

  const renderItem = ({item, index}) => {
    return (
      <View style={styles.listContainer}>
        <View style={styles.listText}>
          <Text style={styles.listDate}>{item.date}</Text>
          <Text style={styles.listItem}>{item.title}</Text>
        </View>
        <Icon
          name="trash-2"
          size={30}
          color="red"
          style={{marginLeft: 'auto', padding: 5}}
          // onPress={deleteTodo}
        />
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.titleHeader}>Masukkan Todo List</Text>
      </View>
      <View style={styles.textInputContainer}>
        <TextInput
          style={styles.textInput}
          placeholder="Input here"
          placeholderTextColor="#abbabb"
          value={state.input}
          onChangeText={(value) => handleChange(value)}
        />
        <TouchableOpacity onPress={() => state.addTodo()}>
          <Icon name="plus" size={30} color="black" style={{marginLeft: 15}} />
        </TouchableOpacity>
      </View>

      <FlatList
        data={state.todos}
        renderItem={renderItem}
        key={state.todos.key}
        deleteTodo={() => deleteTodo(state.todos.key)}
      />
    </View>
  );
};

export default TodoList;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: '#F5FCFF',
    padding: 10,
  },
  header: {
    marginTop: 10,
    paddingBottom: 10,
  },
  titleHeader: {
    fontSize: 16,
    color: 'black',
  },
  textInputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: 'black',
    borderWidth: 1,
    paddingRight: 10,
    color: 'black',
  },
  textInput: {
    flex: 1,
    height: 20,
    fontSize: 18,
    fontWeight: 'bold',
    color: '#000',
    paddingLeft: 10,
    minHeight: '3%',
    height: 40,
  },
  listContainer: {
    marginTop: 15,
    flexDirection: 'row',
    borderColor: '#aaaaaa',
    borderWidth: 1,
    width: '100%',
    alignItems: 'stretch',
    minHeight: 40,
  },
  listText: {
    display: 'flex',
    flexDirection: 'column',
  },
  listDate: {
    paddingLeft: 10,
  },
  listItem: {
    paddingLeft: 10,
    fontSize: 17,
    color: 'black',
  },
});
