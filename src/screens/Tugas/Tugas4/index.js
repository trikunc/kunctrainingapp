import React, {useState, createContext} from 'react';
import {View, Text} from 'react-native';
import TodoList from './TodoList';

export const RootContext = createContext();

const Context = () => {
  const [input, setInput] = useState('');
  const [todos, setTodos] = useState([]);

  const handleChangeInput = (value) => {
    setInput(value);
  };

  const addTodo = () => {
    const day = new Date().getDate();
    const month = new Date().getMonth();
    const year = new Date().getFullYear();
    const today = `${day}/${month}/${year}`;
    const key = Date.now();
    setTodos([...todos, {title: input, date: today, key: key}]);
    setInput('');
  };

  const deleteTodo = (id) => {
    console.log('hoooooooooooooooi', id);
    setTodos(
      todos.filter((todos) => {
        if (todos.key !== id) return true;
      }),
    );
  };

  return (
    <RootContext.Provider
      value={{
        input,
        todos,
        handleChangeInput,
        addTodo,
        deleteTodo,
      }}>
      <TodoList />
    </RootContext.Provider>
  );
};

export default Context;
